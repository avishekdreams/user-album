import * as React from 'react';
import { styled } from '@mui/material/styles';
import { useNavigate } from 'react-router-dom';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

export default function UsersList(props) {
    const { list } = props;
    const navigate = useNavigate();

    const handleClick = (id) => {
        navigate(`/album/${id}`, { state: { 'name': list.name, 'userid': list.id } });
    }

    return (
        <StyledTableRow key={list.name}>
            <StyledTableCell component="th" scope="row" onClick={() => handleClick(list.id)} style={{ cursor:"pointer" }}>{list.name}</StyledTableCell>
            <StyledTableCell onClick={() => handleClick(list.id)} style={{ cursor:"pointer" }}>{list.username}</StyledTableCell>
            <StyledTableCell onClick={() => handleClick(list.id)} style={{ cursor:"pointer" }}>{list.email}</StyledTableCell>
            <StyledTableCell onClick={() => handleClick(list.id)} style={{ cursor:"pointer" }}>{list.phone}</StyledTableCell>
            <StyledTableCell onClick={() => handleClick(list.id)} style={{ cursor:"pointer" }}>{list.website}</StyledTableCell>
        </StyledTableRow>
    )
}