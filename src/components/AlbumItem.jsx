import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

export default function Album(props) {
    return (
        <Card sx={{ maxWidth: 345 }} onClick={() => props.handleClick(props.albm)}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    height="auto"
                    image="/assets/album.png"
                    alt={props.albm.title}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {props.albm.title}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    )
}