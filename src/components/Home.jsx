import React, { useEffect, useState } from 'react';
import { Box, Breadcrumbs, Link, Typography } from '@mui/material';
import axios from 'axios';
import Users from './Users';

const getUsers = async () => {
    const { data } = await axios.get('https://jsonplaceholder.typicode.com/users');
    return data;
}

export default function Home() {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        getUsers().then(setUsers);
    }, []);

    return (
        <div className='container' style={{ width: "80%", margin: "0 auto" }}>
            <h2>List of Users</h2>
            <Breadcrumbs aria-label='breadcrumb' separator={'-'}>
                <Typography color='text.primary'>Home</Typography>
            </Breadcrumbs>
            <Box m={2}>
                {typeof users !== 'undefined' && users.length && (
                    <Users users={users} />
                )}
            </Box>

        </div>
    )
}