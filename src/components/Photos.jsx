import React, { useEffect, useState, useCallback } from 'react';
import axios from 'axios';
import { useParams, useLocation } from "react-router-dom";
import { Breadcrumbs, Link, Typography } from '@mui/material';

export default function Photos(props) {
    const { id } = useParams();
    const { state } = useLocation();
    const [photos, setPhotos] = useState([]);

    const getPhotos = useCallback(async () => {
        const response = await axios.get(`https://jsonplaceholder.typicode.com/albums/${id}/photos`);
        setPhotos(response.data);
    }, [id]);

    useEffect(() => {
        getPhotos();
    }, [getPhotos]);

    return (
        <div className='container' style={{ width: "80%", margin: "0 auto" }}>
            <h2>{state.name} has {photos.length} photos</h2>
            <Breadcrumbs aria-label='breadcrumb' separator={'-'}>
                <Link underline='hover' href='/'>Home</Link>
                <Link underline='hover' href={`/album/${state.user}/?user=${state.name}&usrid=${state.user}`}>Album</Link>
                <Typography color='text.primary'>Photos</Typography>
            </Breadcrumbs>

            <div className="image-grid">
                {photos.length && photos.map((val) => (
                    <img src={val.thumbnailUrl} alt="" className='grid-image' key={val.id} />
                ))}
            </div>
        </div>
    )
}