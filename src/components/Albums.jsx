import React, { useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import { useParams, useLocation, useNavigate } from "react-router-dom";
import { Box, Breadcrumbs, Link, Typography } from '@mui/material';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import './css/Album.css';
import Album from './AlbumItem';

export default function Albums() {
    const { id } = useParams();
    const useQuery = () => new URLSearchParams(useLocation().search);
    const query = useQuery();
    const navigate = useNavigate();
    const { state } = useLocation();
    const [albums, setAlbums] = useState([]);

    
    const responsive = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 5
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 3
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1
        }
    };

    const getAlbums = useCallback(async () => {
        const response = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}/albums`);
        setAlbums(response.data);
    }, [id]);

    useEffect(() => {
        getAlbums();
    }, [getAlbums]);

    const handleClick = (event) => {        
        navigate(`/photos/${event.id}`, {
            state: {
                name: state !== null ? state.name : query.get('user'),
                user: state !== null ? state.userid : query.get('usrid'),
            }
        });
    }

    return (
        <div className='container' style={{ width: "80%", margin: "0 auto" }}>
            <Box className="boxcontainer">
                <Typography variant="h3" gutterBottom>
                    {state !== null ? state.name : query.get('user')} has {Object.keys(albums).length} albums
                </Typography>

                <Breadcrumbs aria-label='breadcrumb' separator={'-'}>
                    <Link underline='hover' href='/'>Home</Link>
                    <Typography color='text.primary'>Albums</Typography>
                </Breadcrumbs>

                <Carousel responsive={responsive}>
                    {albums.map((val) => (
                        <Album albm={val} handleClick={handleClick} key={val.id} />
                    ))}
                </Carousel>
            </Box>
        </div>

    )
}