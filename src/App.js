import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import Albums from './components/Albums';
import Photos from './components/Photos';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" exact element={<Home />} />
          <Route path="/album/:id" element={<Albums />} />
          <Route path="/photos/:id" element={<Photos />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
